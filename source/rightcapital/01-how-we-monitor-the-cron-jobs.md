---
title: DevOps 自动化实践 - 定时任务监控的进化之路
---

## 背景

自从我司采用 Cronitor 监控定时任务，并使用 Terraform 将监控配置的「代码化」已经有一段时间了。相比于常驻的服务，监控定时任务需要关注的往往并不是进程是否持续运行、是否正常接收请求，而是：

- 是否在既定时间启动？
- 运行过程中是否发生错误？
- 进程是否正常结束？
- 执行时长较往常是否有激增或是骤减？

## 上古时代

几年前，我们在定时任务 **结束时** 放置了一个钩子，这个钩子会将定时任务的成功或失败消息发送到 Slack Channel。

那时团队需要安排一名工程师值班每天查看 Slack 消息通知来判断 Cron Jobs 的结果是否正常。那时常常发生的事情是定时任务中途发生了 crash，导致不会发送失败消息到 Slack，所以我们还需要数 Channel 中成功消息的数量，如果数量和预期不对，再从所有的成功消息做排他法找到失败的任务。最初这种方法是奏效的，因为定时任务的数量非常少，但随着任务越来越多，当我们有十几个定时任务的时候，依赖肉眼查看的成本和人为失误的概率就很高了，而且工程师们也受够了这种重复枯燥的劳动。

## 引入 Cronitor

于是我们引入了 [Cronitor](https://cronitor.io/) 来改进这套流程。它的核心功能大概是这样：

1. 通过 UI 或 API 创建定时任务监控项；包括 Cron Jobs 应当开始的时间、可容忍的阈值等。
2. 在任务开始结束和发生异常时，通过 HTTP API 发起 ping 请求向 Cronitor 上报。
3. 如果任务未在预期的时间启动或结束，或收到了 fail ping，则认为任务出错；
4. 将失败或错误的任务通知到 Slack 或 Webhook 等。

如此一来，Crontiro 只会将我们关注的错误通知发送到 Slack，我们的工作就从 **关注成功消息，找出失败到那一个** 就变成了 **关注错误消息**，显著降低了人力负担。

Cronitor 支持的 Integrations 有 Slack、Pagerduty、Opsgenie、VictorOps 等，我们配置了 Slack。

![](/resources/4f4faba0d1a9ba276cdb29af73ef91ac.png)

另外，在 Cronitor 的 Web UI 上也能一目了然地看到各个 Cron Jobs 的状态。

![](/resources/6600da225b1a69f142d384a330cef08e.png)

> 如果你的服务在国内，或者想找个免费的方案，类似的开源产品有 [healthchecks.io](https://healthchecks.io/)

## 责任到人

到了前面这个阶段，我们已经显著降低了人力成本。可还是需要一个人值班去关注 slack 中的失败消息，并将错误转给定时任务的负责人处理。而随着公司的发展，定时任务的数量和同事越来越多，负责盯消息的人经常不知道这个错误改交给谁处理，而摊上值班的人也都不喜欢这个工作。

由于 Cronitor 并没有责任人这个概念，我们利用 Cronitor 的 Tag 和 Webhook 做出了自己的解决方案。

从上图顶部的一排 Tags 中可以发现有一些名为 `owner:*` 的 Tag，我们为定时任务附上这样的 Tag。同时，把原先的 Slack Integration 改为使用 Webhook，将失败的任务信息统一发送至内部开发的一款名为 `Cronitor Failure Dispatcher` 的小工具，由它将失败通知发送到 Slack Channel。该工具在发送通知前会读取任务的 Tags，并在通知信息中 Mention 指定的人员，例如 `@annie.wang`。

这样我们便达到了 **责任到人**，彻底将倒霉的值班工程师从这枯燥的工作中解救了出来。

![](/resources/6bb504008dd8032c784840570d0d4cb6.png)

## 批量管理

**关注定时任务执行结果** 的工作流程完全自动化了，可创建和更新定时任务监控项仍需人工在 Crontior Web UI 上操作。这在少量 Cron Jobs 时简单易行，但当我们的定时任务已经达到了数十个，且我们存在 Development、Staging、UAT、Production 四套运行环境，相当于本就数量庞大的定时任务需要通过 UI 对应地、各创建四个监控项。这意味着：

1. 工作量大，并且存在人为失误的可能性（例如同一监控项的各个环境的配置不统一）；尤其是随着时间推移，后续创建的监控项配置很有可能与先前创建的存在细微的差异，这一点让我们非常头疼。
2. 难以 Review，在我们公司做任何变更都需要通过 review 环节，而通过 Web UI 很难实现 review 的流程。
2. 不可追溯变更历史，不易找出变更的发起者和原因。
3. 难以实施全局改动（例如增加一套环境、修改所有监控项的配置等）。

#### 引入 Terraform

受到 Infrastructure as Code 思想的启发，我们在想：可否实现 **Monitors as Code** 呢？答案是肯定的。

在我们长期将基础设施代码化的过程中，已经学习和了解了 Terraform。它的设计思想允许通过代码声明几乎任何可增删改的「资源」，只需要编写相应的 Providers，定义好资源和它的增删改方法。Terraform 便能够帮助你使用代码声明来管理好这些资源。

![](/resources/b178137d6b5a66bf17ff8484f84ed72a.png)

Providers 的工作就是与具体的资源提供者交互，例如调用 HTTP API、修改数据库记录，甚至是编辑文件。

鉴于 Cronitor 官方和社区并没有提供 Terraform Provider，因此我们自己写了一个并开源了：<https://github.com/nauxliu/terraform-provider-cronitor>。我们终于能能使用代码来管理 Cronitor 的配置了。

以下是一个简单的例子：

```terraform
terraform {
  backend "local" {
    path = "terraform.tfstate"
  }
}

variable "cronitor_api_key" {
  description = "The API key of Cronitor."
}

provider "cronitor" {
  api_key = var.cronitor_api_key
}

resource "cronitor_heartbeat_monitor" "monitor" {
  name = "Test Monitor"

  notifications {
    webhooks = ["https://example.com/"]
  }

  tags = [
    "foo",
    "bar",
  ]

  rule {
    # Every day in 8:00 AM
    value         = "0 8 * * *"
    grace_seconds = 60
  }
}
```

> 小提示：推荐 [crontab.guru](https://crontab.guru/)，是一款可以将 Cron Expression 解析为人类可读语言的小工具。

为了能够安全地将以上内容提交到代码管理系统，我们还将 Cronitor 的 API Key 解藕，使用 Terraform 的 Variables 赋值；这样既可以将它存储到 `terraform.tfvars` 文件中，又能够通过环境变量设置，方便 CI/CD。

实际的代码当然没有这么简单，我们将多个项目所需要的监控项编写成 Terraform Modules 以便于复用，再将各个环境抽象为 Terraform Workspaces，在 Workspaces 内使用 `modules` 指令，根据需要引用模块即可；即使部分项目不存在某些环境也能够灵活编排。

最后，我们将所有的代码存放在一个 Git 仓库中，并结合 GitLab CI 实现了完整的 **从提交代码到实际变更的自动化**。

![](/resources/6e7b3978886440d062046a5c055e3cab.png)

## 最终的工作流

```
                   +--------------+
                   | Users commit |
                   +------+-------+
                          |
                          v
                     +----+-----+
                     | Git repo |
                     +----+-----+
                          |
                          | CI/CD <---> Terraform Manage
                          v
+-----------+     +-------+-----------+
| Cron Jobs +--ping--> Cronitor Monitors |
+-----------+     +-------+-----------+
                          |
                          | Webhook
                          v
              +-----------+-----------------+
              | Cronitor Failure Dispatcher |
              +--------+----+---+-----------+
                       |    |   |
               +-------+    |   +---------+
               |            |             |
               v            v             v
            User A        User B        User C
```
