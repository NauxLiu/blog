---
title: "I'm Wi1dcard."
comments: false
---

> 我的 [在线简历](https://wi1dcard.dev/resume/) / [获取 PDF 版本](wi1dcard.pdf)。

<!--more-->

@card{

### 自我介绍

你好，我是 **Wi1dcard**，一名生于 1998 年但具备 5 年工作经验的软件工程师。

从 2010 年接触 Pascal 编程、参加竞赛起，即对代码产生了极为浓厚的兴趣。由此自学 MFC 和 WinForm 开发，并在 2013 年开始接触外包项目。2015 年起进入职场并逐渐转型 PHP 与后端方向，现就职于某外企从事 [**DevSecOps**](https://www.redhat.com/en/topics/devops/what-is-devsecops) 以及 IT 相关工作。

如果用一个词来形容我，我认为应当是 「**个性**」。我的职业规划大多基于兴趣驱动，我喜欢做与众不同或是未曾有人尝试过的事。我热爱钻研，善于规划并独立思考问题的更优解；但我有一点强迫症，有时会把自己推到极限。我乐于高效地协作或提供帮助，但我不喜欢被打扰 —— 写代码时习惯戴着耳机听电音，这能让我专注。

我热爱软件行业，享受写代码带来的成就感，希望能成为一名「**终身编程者**」，这是我的 [MBTI 性格报告](https://www.16personalities.com/ch/intj-%E4%BA%BA%E6%A0%BC)。

你可以通过 `wi1dcard.cn@gmail.com` 找到我，我将在工作日（UTC+8）的 48 小时内与您取得联系。

期待与你合作。

}

@column-2{

@card{

## 对您的期望

我理想中所处的团队是这样的：

1. 盈利状态。
2. 热情，年轻，开放。
3. 鼓励创新，适当放权，结果导向。
4. 鼓励工程师使用 macOS 或 Linux。

}

@card{

### 外部链接

- [GitHub Repositories](https://github.com/wi1dcard?utf8=%E2%9C%93&tab=repositories&q=&type=source&language=)
- [GitLab Profile](https://gitlab.com/wi1dcard)
- 技术博客: <https://wi1dcard.dev/>

}

}

## 自我修养

@card{

- 高标准自我要求；日常工作环境 macOS + Ubuntu，习惯 CLI，不使用 Windows，不使用盗版软件。
- 良好的 Testing, Documentation, Capitalization, Commit Message, Commit Tree 习惯。
- 坚持写技术博客；以总结工作中的经验教训，同时锻炼表达能力。
- 重度 Google 用户；具备相关知识和技术，并不是「翻墙娱乐圈」中的一员。
- 社区活跃，积极参与开源；[我的 Pull Requests](https://github.com/pulls?utf8=%E2%9C%93&q=is%3Apr+sort%3Aupdated-desc+author%3Awi1dcard)。
- 喜爱英语，至少不是短板；[LearnKu 翻译审阅者](https://learnku.com/users/32249/translations)。
- 正努力成为 [T 型人才](https://en.wikipedia.org/wiki/T-shaped_skills)。

}

## 技能列表

@card{

- 具备丰富 Git Workflow 和 CI/CD 实践经验，熟练使用 GitLab CI、Ansible、Terraform、Helm 等。
- 具备监控预警和 On-Call 经验，了解 Prometheus、Grafana、NewRelic、StatusCake、OpsGenie 等。
- 熟悉 PHP 及周边生态，具备丰富 PHP-FPM + Nginx 运维经验；参与虚拟机到 Kubernetes 的应用改造，目前仍负责公司 Composer 私有仓库以及 PHP-CS-Fixer、PHPStan 等工具类项目的集成和维护。
- 了解网络相关知识，具备 BIRD、IPTables、IPSec、WireGuard、UBNT 全家桶运维经验，了解 HTTP 协议。
- 了解 Office 365 for Business 及相关产品，具备 Azure AD 运维经验，熟练通过 Ticket、邮件、Slack 等方式为美国同事提供 IT Support。
- 熟悉 AWS 和 GCP，阿里云国际版也稍有了解。
- 关注应用安全。
- 正在深入学习 Kubernetes。

}

## 工作经历

@card{

| 公司 / 组织          | 岗位                                  | 时间                   |
| -------------------- | ------------------------------------- | ---------------------- |
| [RightCapital]       | DevSecOps 工程师 + IT 工程师          | 2018年11月 ~           |
| [浙江禾匠信息科技]   | 高级 PHP 工程师                       | 2018年5月 ~ 2018年10月 |
| [浙江椒图科技]       | .NET 工程师 -> PHP 工程师 -> 技术管理 | 2017年2月 ~ 2018年5月  |
| [长沙异次元网络科技] | C++ 工程师                            | 2015年5月 ~ 2016年12月 |

}

[RightCapital]: /employment-history/#RightCapital
[浙江禾匠信息科技]: /employment-history/#浙江禾匠信息科技
[浙江椒图科技]: /employment-history/#浙江椒图科技
[长沙异次元网络科技]: /employment-history/#长沙异次元网络科技

## 我的不足

@card{

- 不了解前端和最新技术，暂时没有这方面的学习计划。
- 不具备对数据库的调优经验，没有实际参与过相关工作。

}

}
